package com.example.ambro.yugicards;

import java.util.ArrayList;

/**
 * Created by ambro on 19/12/17.
 */

public class Cards {
    private String name;
    private String imgPath;
    private String text;
    private String type;
    private String specie;
    private ArrayList<String> types;
    private  String atk;
    private String def;
    private String stars;

    public Cards() {
    }

    public Cards(String name) {
        this.name = name;
    }

    public Cards(String name, String imgPath, String text, String type, String specie, ArrayList<String> types, String atk, String def, String stars) {
        this.name = name;
        this.imgPath = imgPath;
        this.text = text;
        this.type = type;
        this.specie = specie;
        this.types = types;
        this.atk = atk;
        this.def = def;
        this.stars = stars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public ArrayList<String> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<String> types) {
        this.types = types;
    }

    public String getAtk() {
        return atk;
    }

    public void setAtk(String atk) {
        this.atk = atk;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    @Override
    public String toString() {
        return "Cards{" +
                "name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", specie='" + specie + '\'' +
                ", types=" + types +
                ", atk='" + atk + '\'' +
                ", def='" + def + '\'' +
                ", stars='" + stars + '\'' +
                '}';
    }
}
