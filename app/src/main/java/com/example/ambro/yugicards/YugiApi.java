package com.example.ambro.yugicards;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ambro on 19/12/17.
 */

public class YugiApi {
    private final String BASE_URL = "https://www.ygohub.com/api";

    ArrayList<Cards> finalCartas = new ArrayList<>();

    ArrayList<Cards> getListaCartas() {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("all_cards")
                .build();
        String url = builtUri.toString();

        try {
            String JasonResponse = HttpUtils.get(url);

            return primerProcesarJsonp(JasonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    ArrayList<Cards> primerProcesarJsonp(String jsonResponse) {

        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCards = data.getJSONArray("cards");
            for (int i = 0; i < 20; i++) {
                String name = jsonCards.getString(i);

                finalCartas.add(getCartasDetail(name));
                Log.d("CARTA",finalCartas.get(i).toString());
            }
            return finalCartas;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    Cards getCartasDetail(String name) {
        Uri builtUrl = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("card_info")
                .appendQueryParameter("name", name)
                .build();
        String url = builtUrl.toString();
        try {
            String JasonResponse = HttpUtils.get(url);

            Log.d("PENE",JasonResponse);

            return segundoProcesarJsonp(JasonResponse);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    Cards segundoProcesarJsonp(String jsonResponse) {
        try {
            JSONObject data = new JSONObject(jsonResponse);

            Log.d("PENE",data.toString());

            JSONObject jsonCard = data.getJSONObject("card");

            Cards carta = new Cards();
            carta.setName(jsonCard.getString("name"));
            if(jsonCard.has("attack")  || jsonCard.has("defense")) {
                carta.setAtk(jsonCard.getString("attack"));
                carta.setDef(jsonCard.getString("defense"));
            } else {
                carta.setAtk("none");
                carta.setDef("none");
            }
            //carta.setImgPath(jsonCard.getString(""));
            if(jsonCard.has("species")) {
                carta.setSpecie(jsonCard.getString("species"));
            }
            if(jsonCard.has("starts")) {
                carta.setStars(jsonCard.getString("stars"));
            }
            carta.setText(jsonCard.getString("text"));
            carta.setType(jsonCard.getString("type"));
            //carta.setTypes(jsonCard.getString(""));


            return carta;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
